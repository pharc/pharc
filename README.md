 pharc (phlog archiver) automatically archives your phlog posts on a yearly basis as long as 
 they have been created with 'mkphlog' v.0.2 (or higher) or alternatively you 
 have your posts inside directories named in the form $(date +%m-%d-%y).
 
 The script assumes that you have a link in your $HOME to your gopher 
 server. More exactly to $HOME/gopher/phlog. Otherwise it will print a message 
 saying: "No working directory found." As a workaround you can export WORKDIR=
 with the "/path/to/phlog". See wiki at <https://gitlab.com/pharc/pharc/wikis/home>